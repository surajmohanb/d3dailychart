import moment from "moment";
/*
export const generateRandomSleepAwakeData = () => {
    let currentDay = moment().format('YYYY-MM-DD');
    let array = [];
    for (let h = 1; h <= 24; h++) {
        for (let m = 0; m < 60; m++) {
            var sDate = moment().utcOffset(0);
            sDate.set({ hour: h, minute: m, second: 0, millisecond: 0 });
            var eDate = moment().utcOffset(0);
            eDate.set({ hour: h, minute: m + 1, second: 0, millisecond: 0 });

            const rndStatus = Math.ceil(Math.random() * 3);
            let sleepStatus;
            switch (rndStatus) {
                case 1:
                    sleepStatus = "AWAKE";
                    break;
                case 2:
                    sleepStatus = "ASLEEP";
                    break;
                case 3:
                    sleepStatus = "DEFAULT";
                    break;
            }
            const jsonObject = {
                "_id": "5a33fed3fd559e16f10e17a2",
                "sleepStatus": sleepStatus,
                "start": sDate.toISOString(),
                "end": eDate.toISOString(),
                "utcOffset": 21600000,
                "duration": 60,
                "seq": 1159,
                "timeOffset": 0,
                "clientStart": 1513356900000.0,
                "createAt": "2017-12-15T14:56:51.851+0000",
                "userId": "59f9fe55a6871a8d35f75433"
            }
            array.push(jsonObject);
        }
    }
    return array;
}
*/
const generateWeekDays = (day, currentDay) => {
    var new_date = moment(currentDay, 'YYYY-MM-DD');
    new_date.add(day, 'days');
    return new_date.format('YYYY-MM-DD');
}

export const generateRandomSleepAwakeRange = () => {
    let currentDay = moment(new Date()).format('YYYY-MM-DD');
    let array = [];

    for(let d = 0; d < 7; d++){       
        const dayInfo = generateWeekDays(d, currentDay);
        for (let h = 1; h <= 24; h++) {
            for (let m = 0; m < 60; m++) {
                var sDate = moment(dayInfo).utcOffset(0);
                sDate.set({ hour: h, minute: m, second: 0, millisecond: 0 });
                var eDate = moment(dayInfo).utcOffset(0);
                eDate.set({ hour: h, minute: m + 1, second: 0, millisecond: 0 });
    
                const rndStatus = Math.ceil(Math.random() * 3);
                let sleepStatus;
                switch (rndStatus) {
                    case 1:
                        sleepStatus = "AWAKE";
                        break;
                    case 2:
                        sleepStatus = "ASLEEP";
                        break;
                    case 3:
                        sleepStatus = "DEFAULT";
                        break;
                }
                const jsonObject = {
                    "_id": "5a33fed3fd559e16f10e17a2",
                    "sleepStatus": sleepStatus,
                    "start": sDate.toISOString(),
                    "end": eDate.toISOString(),
                    "utcOffset": 21600000,
                    "duration": 60,
                    "seq": 1159,
                    "timeOffset": 0,
                    "clientStart": 1513356900000.0,
                    "createAt": "2017-12-15T14:56:51.851+0000",
                    "userId": "59f9fe55a6871a8d35f75433"
                }
                array.push(jsonObject);
            }
        }
    }
    return array;
}