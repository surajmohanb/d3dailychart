import React, {
    Component
} from 'react';
import * as d3 from "d3";
import moment from "moment";

import json from './data.json';
import { generateRandomSleepAwakeData, generateRandomSleepAwakeRange } from './randomData';

class WeeKViewChart extends Component {

    constructor(props) {
        super(props);
        this.data = null;
        this.state = {
            activeStatus: 'ASLEEP'
        }
        this.statusColor = {
            'AWAKE': ["#99E3BA", "#6ED79D", "#4FAD7A", "#3E875F", "#2C6044", "#12271B"],
            'ASLEEP': ["#F49992", "#F17C73", "#EE6055", "#AE463E", "#6D2C27", "#2C1210"]
        }
    }

    componentDidMount() {
        const jsonData = generateRandomSleepAwakeRange();
        const jData = jsonData.map(function (obj) {
            let nObj = {
                day: moment(obj.start).utc().day(),
                hour: moment(obj.start).utc().hours(),
                startMin: moment(obj.start).utc().minute(),
                endMin: moment(obj.end).utc().minute(),
                value: obj.sleepStatus
            }           
            return nObj;
        });
        this.processData(jData);
    }

    processData(json) {
        const data = [],
            days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
            times = ["1a", "2a", "3a", "4a", "5a", "6a", "7a", "8a", "9a", "10a", "11a", "12a", "1p", "2p", "3p", "4p", "5p", "6p", "7p", "8p", "9p", "10p", "11p", "12p"];

        this.data = json;
        this.days = days;
        this.times = times;
        this.drawChart();
    }

    drawChart() {
        const data = this.data;
        const days = this.days;
        const times = this.times;
        const statusColor = this.statusColor;
        const activeStatus = this.state.activeStatus;
        const margin = {
            top: 30,
            right: 0,
            bottom: 100,
            left: 75
        },
            width = 960 - margin.left - margin.right,
            height = 430 - margin.top - margin.bottom,
            gridSize = width / 24,
            legendElementWidth = gridSize * 2,
            buckets = 9,
            colors = {
                'DEFAULT': "#ffffd9",
                'ASLEEP': "#d83131",
                'AWAKE': "#2889e2"
            };

        const statusData = data.filter((o) => o.value === 'activeStatus');

        const svg = d3.select("#heat-map svg")
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
            .append("g")
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

        svg.selectAll(".dayLabel")
            .data(days)
            .enter().append("text")
            .text(function (d) {
                return d;
            })
            .attr("x", 0)
            .attr("y", function (d, i) {
                return i * gridSize;
            })
            .style("text-anchor", "end")
            .attr("transform", "translate(-6," + gridSize / 1.5 + ")")
            .attr("class", function (d, i) {
                return ((i >= 0 && i <= 4) ? "dayLabel mono axis axis-workweek" : "dayLabel mono axis");
            });

        svg.selectAll(".timeLabel")
            .data(times)
            .enter().append("text")
            .text(function (d) {
                return d;
            })
            .attr("x", function (d, i) {
                return i * gridSize;
            })
            .attr("y", height - 20)
            .style("text-anchor", "middle")
            .attr("transform", "translate(" + gridSize / 2  + ", -6)")
            .attr("class", function (d, i) {
                return ((i >= 7 && i <= 16) ? "timeLabel mono axis axis-worktime" : "timeLabel mono axis");
            });


        var div = d3.select("body").append("div")
            .attr("class", "tooltip tooltip-heap")
            .style("opacity", 0);


        var xScaleRange = d3.scaleLinear()
            .domain([1, 1440])
            .range([0, width]);

        const cards = svg.selectAll(".hour")
            .data(data, function (d) {
                return d;
            });
        cards.enter().append("rect")
            .attr("x", function (d, i) {  
                let hMin = Math.floor(d.hour * 60) + d.endMin;
                const offset = xScaleRange(hMin);
                return offset;
            })
            .attr("y", function (d) {
                return (d.day) * gridSize;
            })
            .attr("rx", 0)
            .attr("ry", 0)
            .attr("width", (gridSize / 60))
            .attr("height", gridSize)
            .style("fill", function (d) {
                let index = 0;
                if (d.value == 'DEFAULT') {
                    return colors['DEFAULT'];
                }
                return statusColor[d.value][index];
            })
            .on("mouseover", function (d, i) {
                if (d.value !== 'DEFAULT' && d.endMin) {
                    div.transition()
                        .duration(200)
                        .style("opacity", .9);
                    div.html(days[d.day] + ': ' + (d.hour + 1) + ' : ' + (d.endMin) + ' min(s)')
                        .style("left", (d3.event.pageX) + "px")
                        .style("top", (d3.event.pageY - 28) + "px");
                }

            })
            .on("mouseout", function (d) {
                if (d.value !== 'DEFAULT' && d.endMin) {
                    div.transition()
                        .duration(500)
                        .style("opacity", 0);
                }

            });
    }

    render() {
        return (
            <React.Fragment>
                <div id="heat-map">
                    <svg />
                </div>
            </React.Fragment>

        )
    }
}

export default WeeKViewChart;